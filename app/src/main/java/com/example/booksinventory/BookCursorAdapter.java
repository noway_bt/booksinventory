package com.example.booksinventory;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.booksinventory.data.StoreContract.BookEntry;

public class BookCursorAdapter extends CursorAdapter {
    public BookCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find individual views that we want to modify in the list item layout.
        TextView nameTextView = (TextView) view.findViewById(R.id.name);
        TextView summaryTextView = (TextView) view.findViewById(R.id.summary);
        TextView priceTextView = (TextView) view.findViewById(R.id.price);

        int position = cursor.getPosition();
        Button saleButton = view.findViewById(R.id.sale_button);
        saleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cursor.moveToPosition(position);
                int quantityColumnIndex = cursor.getColumnIndex(BookEntry.COLUMN_PRODUCT_QUANTITY);
                int bookQuantity = cursor.getInt(quantityColumnIndex);
                if (bookQuantity < 1) {
                    Toast.makeText(context, R.string.empty_store, Toast.LENGTH_SHORT).show();
                    return;
                }
                bookQuantity--;

                ContentValues values = new ContentValues();
                values.put(BookEntry.COLUMN_PRODUCT_QUANTITY, String.valueOf(bookQuantity));

                String selection = BookEntry._ID + "=?";
                int columnID = cursor.getColumnIndex(BookEntry._ID);
                int item_id = cursor.getInt(columnID);
                String itemIDArgs = Integer.toString(item_id);
                String[] selectionArgs = {itemIDArgs};
                int returnCode = context.getContentResolver().update(
                        Uri.withAppendedPath(BookEntry.CONTENT_URI, Integer.toString(item_id)),
                        values, selection, selectionArgs);
                if (returnCode == 0) {
                    Toast.makeText(context, R.string.sale_failed, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, R.string.sold, Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Find the columns of book attributes that we're interested in.
        int nameColumnIndex = cursor.getColumnIndex(BookEntry.COLUMN_PRODUCT_NAME);
        int quantityColumnIndex = cursor.getColumnIndex(BookEntry.COLUMN_PRODUCT_QUANTITY);
        int priceColumnIndex = cursor.getColumnIndex(BookEntry.COLUMN_PRODUCT_PRICE);

        // Read the book attributes from the Cursor for the current book.
        String bookName = cursor.getString(nameColumnIndex);
        String bookQuantity = cursor.getString(quantityColumnIndex);
        double bookPrice = cursor.getDouble(priceColumnIndex);

        // Update the TextViews with the attributes for the current book.
        nameTextView.setText(bookName);
        summaryTextView.setText(bookQuantity);
        priceTextView.setText(String.valueOf(bookPrice) + " " + context.getString(R.string.currency));
    }
}
